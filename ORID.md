# Daily Report (2023/07/27)

## O

### Code Review

1. #### Yesterday, my homework did not implement the responsive function. In today’s Code Review session, I learned the codes of many classmates. Each student’s implementation method is different. In today’s spare time, I modified my code to realize the responsive function.

2. #### Today's main learning task is to build a back-end implementation for yesterday's front-end. The whole process is relatively smooth, but there will be problems when using Flyway to manage the database version. Different databases have different requirements for SQL, so after switching databases, problems are likely to occur. Pay special attention to this when using

## R

#### meaningful

## I

#### The most meaningful activity is sharing elevator speech.

## D

1. #### At this stage, I feel like Flyway is causing more problems than it solves.

2. #### Still confused when writing CSS styles.