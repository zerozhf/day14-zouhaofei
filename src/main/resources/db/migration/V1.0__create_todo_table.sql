create table if NOT EXISTS todo(
                            `ID` bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
                            `NAME` varchar(255) NOT NULL,
                            `DONE` BOOLEAN
)