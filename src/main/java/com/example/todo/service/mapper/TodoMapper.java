package com.example.todo.service.mapper;

import com.example.todo.entity.Todo;
import com.example.todo.service.dto.TodoRequest;
import com.example.todo.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public TodoMapper() {
    }

    public static Todo toEntity(TodoRequest todoRequest){
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest,todo);
        return todo;
    }

    public static TodoResponse toResponse(Todo todo){
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo,todoResponse);
        return todoResponse;
    }


}
