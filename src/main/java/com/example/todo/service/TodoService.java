package com.example.todo.service;

import com.example.todo.entity.Todo;
import com.example.todo.repository.TodoJpaRepository;
import com.example.todo.service.dto.TodoRequest;
import com.example.todo.service.dto.TodoResponse;
import com.example.todo.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoJpaRepository todoJpaRepository;

    public TodoService(TodoJpaRepository todoJpaRepository) {
        this.todoJpaRepository = todoJpaRepository;
    }

    public List<TodoResponse> findAll() {
        return todoJpaRepository.findAll().stream().map(TodoMapper::toResponse).collect(Collectors.toList());
    }

    public TodoResponse save(TodoRequest todoRequest){
        Todo todo =todoJpaRepository.save(TodoMapper.toEntity(todoRequest));
        return TodoMapper.toResponse(todo);
    }


    public TodoResponse updateTodo(Long id, TodoRequest todoRequest) {
        Todo toBeUpdatedTodo = todoJpaRepository.findById(id).get();
        if(Objects.nonNull(todoRequest.getDone())){
            toBeUpdatedTodo.setDone(todoRequest.getDone());
        }
        if(Objects.nonNull(todoRequest.getName())){
            toBeUpdatedTodo.setName(todoRequest.getName());
        }
        return TodoMapper.toResponse(todoJpaRepository.save(toBeUpdatedTodo));
    }

    public void deleteTodo(Long id) {
        todoJpaRepository.deleteById(id);
    }

    public TodoResponse getTodoById(Long id) throws Exception {
        return TodoMapper.toResponse(todoJpaRepository.findById(id).orElseThrow(Exception::new));

    }
}
