package com.example.todo.controller;

import com.example.todo.entity.Todo;
import com.example.todo.repository.TodoJpaRepository;
import com.example.todo.service.TodoService;
import com.example.todo.service.dto.TodoRequest;
import com.example.todo.service.dto.TodoResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoJpaRepository todoJpaRepository;

    @BeforeEach
    void setUp() {
        todoJpaRepository.deleteAll();
    }

    private static TodoRequest getTestTodoRequest() {
        return new TodoRequest("test", false);
    }

    private static Todo getTestTodo() {
        return new Todo(null,"test", false);
    }

    @Test
    void should_return_a_List_with_TodoResponse_when_perform_getAllTodos() throws Exception {
        Todo todo = todoJpaRepository.save(getTestTodo());
        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(todo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo.getDone()));

    }


    @Test
    void should_return_a_todoResponse_when_perform_createTodo_given_a_todoRequest() throws Exception {
        TodoRequest todoRequest = getTestTodoRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJson = objectMapper.writeValueAsString(todoRequest);
        mockMvc.perform(post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todoRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoRequest.getDone()));
    }

    @Test
    void should_return_a_todoResponse_when_perform_updateTodo_given_a_todoRequest_with_new_done() throws Exception {
        Todo todo = todoJpaRepository.save(getTestTodo());
        TodoRequest todoRequest = new TodoRequest();
        todoRequest.setDone(!todo.getDone());
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJson = objectMapper.writeValueAsString(todoRequest);
        mockMvc.perform(put("/todo/{id}",todo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todoRequest.getDone()));
    }


    @Test
    void should_return_a_todoResponse_when_perform_updateTodo_given_a_todoRequest_with_new_name() throws Exception {
        Todo todo = todoJpaRepository.save(getTestTodo());
        TodoRequest todoRequest = new TodoRequest();
        todoRequest.setName("hello");
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequestJson = objectMapper.writeValueAsString(todoRequest);
        mockMvc.perform(put("/todo/{id}",todo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todoRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.getDone()));
    }

    @Test
    void should_return_void_when_perform_deleteTodo_given_id() throws Exception {
        Todo todo = todoJpaRepository.save(getTestTodo());
        mockMvc.perform(delete("/todo/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));
        assertTrue(todoJpaRepository.findById(todo.getId()).isEmpty());
    }

    @Test
    void should_return_todoResponse_when_perform_getTodoById_given_id() throws Exception {
        Todo todo = todoJpaRepository.save(getTestTodo());
        System.out.println(todo.getDone());
        mockMvc.perform(get("/todo/{id}", todo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.getDone()));
    }


}
